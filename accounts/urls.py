from django.conf.urls import url, include
from . import views


urlpatterns = [
    # Account
    url(r'^profile/(?P<username>[\w.@+-]+)/$',
        views.profile_view,
        name="account_profile"),
    url(r'^profile/edit/(?P<slug>[\w.@+-]+)/$',
        views.OrpheusUserUpdate.as_view(),
        name='account_edit'),
    url(r'^login/$',
        views.OrpheusLoginView.as_view(),
        name="account_login"),
    url(r'^signup/$',
        views.OrpheusSignupView.as_view(),
        name="account_signup"),
    url(r'^logout/$',
        views.OrpheusLogoutView.as_view(),
        name="account_logout"),
    url(r'^password/change/$',
        views.OrpheusPasswordChangeView.as_view(),
        name="account_change_password"),
    url(r'^password/set/$',
        views.OrpheusPasswordSetView.as_view(),
        name="account_set_password"),
    url(r'^inactive/$',
        views.OrpheusAccountInactiveView.as_view(),
        name="account_inactive"),

    # E-mail
    url(r'^email/$',
        views.OrpheusEmailView.as_view(),
        name="account_email"),
    url(r'^confirm-email/$',
        views.OrpheusEmailVerificationSentView.as_view(),
        name="account_email_verification_sent"),
    url(r'^confirm-email/(?P<key>\w+)/$',
        views.OrpheusConfirmEmailView.as_view(),
        name="account_confirm_email"),

    # Password reset
    url(r'^password/reset/$',
        views.OrpheusPasswordResetView.as_view(),
        name="account_reset_password"),
    url(r'^password/reset/done/$',
        views.OrpheusPasswordResetDoneView.as_view(),
        name="account_reset_password_done"),
    url(r'^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$',
        views.OrpheusPasswordResetFromKeyView.as_view(),
        name="account_reset_password_from_key"),
    url(r'^password/reset/key/done/$',
        views.OrpheusPasswordResetFromKeyDoneView.as_view(),
        name="account_reset_password_from_key_done"),

    url(r'^', include('allauth.urls')),
]
