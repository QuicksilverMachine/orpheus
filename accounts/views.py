from allauth.account import views
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views.generic.edit import UpdateView
from .forms import LoginFormWithoutAutofocus
from .models import OrpheusUser
from videos.models import Video
from music.models import Album


def profile_view(request, username):
    orpheus_user = get_object_or_404(OrpheusUser, username=username)
    user_videos = Video.objects.filter(user=orpheus_user)
    user_albums = Album.objects.filter(user=orpheus_user)
    return render(request, 'accounts/profile.html',
                  {'orpheus_user': orpheus_user,
                   'user_videos': user_videos,
                   'user_albums': user_albums})


class OrpheusUserUpdate(UpdateView):
    model = OrpheusUser
    fields = ['full_name', 'profile_picture']
    slug_field = 'username'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.save()
        if self.request.is_ajax():
            return HttpResponse(user.get_absolute_url())
        else:
            return super(OrpheusUserUpdate, self).form_valid(form)

    def get_object(self, queryset=None):
        user = super(OrpheusUserUpdate, self).get_object()
        if not user == self.request.user and not self.request.user.is_staff:
            raise Http404
        return user


class OrpheusAccountInactiveView(views.AccountInactiveView):
    template_name = 'accounts/account_inactive.html'


class OrpheusEmailView(views.EmailView):
    template_name = 'accounts/email.html'


class OrpheusConfirmEmailView(views.ConfirmEmailView):
    template_name = 'accounts/email_confirm.html'


class OrpheusLoginView(views.LoginView):
    template_name = 'accounts/login.html'
    form_class = LoginFormWithoutAutofocus


class OrpheusLogoutView(views.LogoutView):
    template_name = 'accounts/logout.html'


class OrpheusPasswordChangeView(views.PasswordChangeView):
    template_name = 'accounts/password_change.html'


class OrpheusPasswordResetView(views.PasswordResetView):
    template_name = 'accounts/password_reset.html'


class OrpheusPasswordResetDoneView(views.PasswordResetDoneView):
    template_name = 'accounts/password_reset_done.html'


class OrpheusPasswordResetFromKeyView(views.PasswordResetFromKeyView):
    template_name = 'accounts/password_reset_from_key.html'


class OrpheusPasswordResetFromKeyDoneView(views.PasswordResetFromKeyDoneView):
    template_name = 'accounts/password_reset_from_key_done.html'


class OrpheusPasswordSetView(views.PasswordSetView):
    template_name = 'accounts/password_set.html'


class OrpheusSignupView(views.SignupView):
    template_name = 'accounts/signup.html'


class OrpheusEmailVerificationSentView(views.EmailVerificationSentView):
    template_name = 'accounts/verification_sent.html'
