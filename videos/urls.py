from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)$', views.VideoWatchView.as_view(), name='watch'),
    url(r'^add/$', views.VideoCreate.as_view(), name='add'),
    url(r'^edit/(?P<pk>[0-9]+)$', views.VideoUpdate.as_view(), name='edit'),
    url(r'^delete/(?P<pk>[0-9]+)$',
        views.VideoDelete.as_view(),
        name='delete'),
    url(r'^search/$', views.VideoSearchView.as_view(), name='search'),
    url(r'^frontpage/(?P<page>[0-9]+)$',
        views.get_frontpage_videos,
        name='frontpage'),
]
