var player = null;
var keyboard_skip = 5; //seconds

$('video').mediaelementplayer({
    startLanguage:'en',
    translationSelector: true,
    keyActions: [
        {
            keys: [
                37, // LEFT
                227 // Google TV rewind
            ],
            action: function(player, media) {
                if (!isNaN(media.duration) && media.duration > 0) {
                    if (player.isVideo) {
                            player.showControls();
                            player.startControlsTimer();
                    }

                    // keyboard_skip seconds
                    var newTime = Math.max(media.currentTime - keyboard_skip, 0);
                    media.setCurrentTime(newTime);
                }
            }
        },
        {
            keys: [
                39, // RIGHT
                228 // Google TV forward
            ],
            action: function(player, media) {
                if (!isNaN(media.duration) && media.duration > keyboard_skip) {
                    if (player.isVideo) {
                        player.showControls();
                        player.startControlsTimer();
                    }

                    // keyboard_skip seconds
                    var newTime = Math.min(media.currentTime + 5, media.duration);
                    media.setCurrentTime(newTime);
                }
            }
        }
    ],
    success: function(mediaElement){
        player = mediaElement;
    }
});
