import os
from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django_cleanup.signals import cleanup_post_delete

from accounts.models import OrpheusUser
from media_common.models import Media
from orpheus.settings import STATIC_URL, MEDIA_ROOT
from orpheus.utils import remove_empty_dir


def video_upload_path(instance, filename):
    return os.path.join("videos/videos/%s/" % instance.id, filename)


def caption_upload_path(instance, filename):
    return os.path.join("videos/captions/%s/" % instance.id, filename)


def thumbnail_upload_path(instance, filename):
    return os.path.join("videos/thumbnails/%s/" % instance.id, filename)


class Video(Media):
    source = models.FileField(_("Source"), upload_to=video_upload_path)
    captions = models.FileField(_("Captions"),
                                upload_to=caption_upload_path,
                                blank=True)
    thumbnail = models.FileField(_("Thumbnail"),
                                 upload_to=thumbnail_upload_path,
                                 blank=True)

    def get_absolute_url(self):
        return reverse('videos:watch', kwargs={'pk': self.pk})

    @property
    def thumbnail_url(self):
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return os.path.join(STATIC_URL, 'videos/img/video-default.jpg')


# Receive the pre_delete signal and delete the files
# associated with the model instance.
@receiver(post_delete, sender=Video)
def video_delete(sender, instance, **kwargs):
    cleanup_post_delete.send(sender)
    remove_empty_dir(os.path.join(
        MEDIA_ROOT,
        video_upload_path(instance, "")))
    remove_empty_dir(os.path.join(
        MEDIA_ROOT,
        caption_upload_path(instance, "")))
    remove_empty_dir(os.path.join(
        MEDIA_ROOT,
        thumbnail_upload_path(instance, "")))
