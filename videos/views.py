from django.shortcuts import render

from media_common.views import (
    MediaSearchView,
    MediaDetailView,
    MediaCreate,
    MediaUpdate,
    MediaDelete,
    MediaIndexView
)
from .models import Video


class IndexView(MediaIndexView):
    template_name = 'videos/index.html'
    context_object_name = 'all_videos'

    def get_queryset(self):
        videos = Video.objects.all()
        return MediaIndexView.get_frontpage_media(videos)


def get_frontpage_videos(request, page):
    videos = Video.objects.all()
    page = int(page)
    all_videos = MediaIndexView.get_frontpage_media(videos, page)
    return render(request, 'videos/video-list.html', {'all_videos': all_videos})


class VideoWatchView(MediaDetailView):
    model = Video
    template_name = 'videos/watch.html'


class VideoCreate(MediaCreate):
    model = Video
    fields = ['title', 'description', 'source', 'thumbnail', 'captions', 'tags']


class VideoUpdate(MediaUpdate):
    model = Video
    fields = ['title', 'description', 'thumbnail', 'captions', 'tags']


class VideoDelete(MediaDelete):
    model = Video


class VideoSearchView(MediaSearchView):
    template_name = 'videos/search-result.html'
    model = Video
