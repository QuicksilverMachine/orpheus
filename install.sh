#!/usr/bin/env bash

ORPHEUS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cp ${ORPHEUS_DIR}/init_db.sql /tmp/init_db.sql
sudo -i -u postgres psql -f /tmp/init_db.sql
rm /tmp/init_db.sql

pip3 install -r requirements.txt;
python3 manage.py makemigrations;
python3 manage.py migrate;

cp ${ORPHEUS_DIR}/update_sites.sql /tmp/update_sites.sql
sudo -i -u postgres psql -d orpheus -f /tmp/update_sites.sql
rm /tmp/update_sites.sql

python3 database.py all;
