DROP DATABASE IF EXISTS orpheus;
DROP USER IF EXISTS orpheus_user;
CREATE DATABASE orpheus;
CREATE USER orpheus_user WITH PASSWORD 'twoinharmony';
ALTER ROLE orpheus_user SET client_encoding TO 'utf8';
ALTER ROLE orpheus_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE orpheus_user SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE orpheus TO orpheus_user;
