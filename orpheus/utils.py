import os
import shutil


def remove_dir(path):
    # Check if directory exists
    if os.path.isdir(path):
        shutil.rmtree(path)


def remove_empty_dir(path):
    # Check if directory exists
    if os.path.isdir(path):
        # Check if directory is empty
        if os.listdir(path):
            shutil.rmtree(path)
