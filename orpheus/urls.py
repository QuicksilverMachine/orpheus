from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = i18n_patterns(
    # url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^media/', include('media_common.urls', namespace="media")),
    url(r'^videos/', include('videos.urls', namespace="videos")),
    url(r'^music/', include('music.urls', namespace="music")),
    url(r'^stats/', include('stats.urls', namespace="stats")),
    url(r'^$', views.index, name='index'),
)

urlpatterns += [
    url(r'^i18n/', include('django.conf.urls.i18n')),
]

handler404 = 'orpheus.views.page_not_found_view'
handler500 = 'orpheus.views.error_view'
handler403 = 'orpheus.views.permission_denied_view'
handler400 = 'orpheus.views.bad_request_view'

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
