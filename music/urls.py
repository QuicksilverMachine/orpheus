from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)$', views.AlbumListenView.as_view(), name='listen'),
    url(r'^album/add/$', views.AlbumCreate.as_view(), name='add-album'),
    url(r'^album/edit/(?P<pk>[0-9]+)$', views.AlbumUpdate.as_view(),
        name='edit-album'),
    url(r'^album/delete/(?P<pk>[0-9]+)$', views.AlbumDelete.as_view(),
        name='delete-album'),
    url(r'^song/add/(?P<album_id>[0-9]+)$', views.SongCreate.as_view(),
        name='add-song'),
    url(r'^song/edit/(?P<pk>[0-9]+)$', views.SongUpdate.as_view(),
        name='edit-song'),
    url(r'^song/delete/(?P<pk>[0-9]+)$', views.SongDelete.as_view(),
        name='delete-song'),
    url(r'^search/$', views.AlbumSearchView.as_view(), name='search'),
    url(r'^frontpage/(?P<page>[0-9]+)$',
        views.get_frontpage_videos,
        name='frontpage'),
]
