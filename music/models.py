import os
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django_cleanup.signals import cleanup_post_delete

from accounts.models import OrpheusUser
from media_common.models import Media
from orpheus.settings import STATIC_URL, MEDIA_ROOT
from orpheus.utils import remove_empty_dir


def thumbnail_upload_path(instance, filename):
    return os.path.join("music/thumbnails/%s/" % instance.id, filename)


class Album(Media):
    thumbnail = models.FileField(_("Thumbnail"),
                                 upload_to=thumbnail_upload_path,
                                 blank=True)

    def get_absolute_url(self):
        return reverse('music:listen', kwargs={'pk': self.pk})

    @property
    def thumbnail_url(self):
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return os.path.join(STATIC_URL, 'music/img/album-default.jpg')


# Receive the pre_delete signal and delete the files
# associated with the model instance.
@receiver(post_delete, sender=Album)
def album_delete(sender, instance, **kwargs):
    cleanup_post_delete.send(sender)
    remove_empty_dir(os.path.join(
        MEDIA_ROOT,
        thumbnail_upload_path(instance, "")))


def song_upload_path(instance, filename):
    return os.path.join("music/albums/%s/" % instance.album.id, filename)


class Song(models.Model):
    track_number = models.IntegerField(_("Track number"))
    title = models.TextField(_("Title"))
    source = models.FileField(_("Source"), upload_to=song_upload_path)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('music:listen', kwargs={'pk': self.album.pk})

    def __str__(self):
        return self.title

