from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from media_common.views import (
    MediaSearchView,
    MediaDetailView,
    MediaCreate,
    MediaUpdate,
    MediaDelete,
    MediaIndexView
)
from .models import Album, Song


class IndexView(MediaIndexView):
    template_name = 'music/index.html'
    context_object_name = 'all_albums'

    def get_queryset(self):
        albums = Album.objects.all()
        return MediaIndexView.get_frontpage_media(albums)


def get_frontpage_videos(request, page):
    albums = Album.objects.all()
    all_albums = MediaIndexView.get_frontpage_media(albums, int(page))
    return render(request, 'music/album-list.html', {'all_albums': all_albums})


class AlbumListenView(MediaDetailView):
    model = Album
    template_name = 'music/listen.html'


class AlbumCreate(MediaCreate):
    model = Album
    fields = ['title', 'description', 'thumbnail', 'tags']


class AlbumUpdate(MediaUpdate):
    model = Album
    fields = ['title', 'description', 'thumbnail', 'tags']


class AlbumDelete(MediaDelete):
    model = Album


class SongCreate(CreateView):
    model = Song
    fields = ['title', 'source', 'track_number']

    def form_valid(self, form):
        song = form.save(commit=False)
        song.user = self.request.user
        song.album = Album.objects.get(pk=self.kwargs['album_id'])
        song.save()
        if self.request.is_ajax():
            return HttpResponse(song.get_absolute_url())
        else:
            return super(SongCreate, self).form_valid(form)


class SongUpdate(UpdateView):
    model = Song
    fields = ['title', 'track_number']

    def form_valid(self, form):
        song = form.save(commit=False)
        song.save()
        if self.request.is_ajax():
            return HttpResponse(song.get_absolute_url())
        else:
            return super(SongUpdate, self).form_valid(form)

    def get_object(self, queryset=None):
        song = super(SongUpdate, self).get_object()
        if (not song.album.user == self.request.user and
                not self.request.user.is_staff):
            raise Http404
        return song


class SongDelete(DeleteView):
    model = Song

    def get_success_url(self):
        song = super(SongDelete, self).get_object()
        album = song.album
        return reverse_lazy('music:listen',
                            kwargs={'pk': album.pk})

    def get_object(self, queryset=None):
        song = super(SongDelete, self).get_object()
        if (not song.album.user == self.request.user and
                not self.request.user.is_staff):
            raise Http404
        return song


class AlbumSearchView(MediaSearchView):
    template_name = 'music/search-result.html'
    model = Album
