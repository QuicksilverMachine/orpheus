from django.apps import AppConfig


class MediaCommonConfig(AppConfig):
    name = 'media_common'
