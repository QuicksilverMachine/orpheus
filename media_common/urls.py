from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^upvote/(?P<media_id>[0-9]+)$',
        views.media_upvote_view,
        name='media-upvote'),
    url(r'^downvote/(?P<media_id>[0-9]+)$',
        views.media_downvote_view,
        name='media-downvote'),

    url(r'^comments/get/(?P<media_id>[0-9]+)$',
        views.CommentListAPIView.as_view(),
        name='comments-get'),
    url(r'^comments/post/$',
        views.CommentCreateApiView.as_view(),
        name='comments-post'),
    url(r'^comments/put/(?P<pk>[0-9]+)$',
        views.CommentUpdateAPIView.as_view(),
        name='comments-put'),
    url(r'^comments/delete/(?P<pk>[0-9]+)$',
        views.CommentDeleteAPIView.as_view(),
        name='comments-delete'),
    url(r'^comments/upvote/(?P<comment_id>[0-9]+)$',
        views.comment_upvote_view,
        name='comments-upvote'),
    url(r'^comments/downvote/(?P<comment_id>[0-9]+)$',
        views.comment_downvote_view,
        name='comments-downvote'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
