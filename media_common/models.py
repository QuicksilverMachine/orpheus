from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from taggit.managers import TaggableManager

from accounts.models import OrpheusUser


class Media(models.Model):
    title = models.CharField(_("Title"), max_length=100)
    description = models.TextField(_("Description"))
    created = models.DateTimeField(_("Created"), default=now)
    user = models.ForeignKey(OrpheusUser, on_delete=models.CASCADE)
    upvoted_by = models.ManyToManyField(OrpheusUser,
                                        blank=True,
                                        related_name="media_upvoted_by")
    downvoted_by = models.ManyToManyField(OrpheusUser,
                                          blank=True,
                                          related_name="media_downvoted_by")
    tags = TaggableManager(blank=True,
                           help_text=_("A list of tags separated by commas or spaces."))

    def __str__(self):
        return self.title

    @property
    def view_count(self):
        return MediaVisit.objects.filter(media=self).count()

    @property
    def upvote_count(self):
        return self.upvoted_by.count()

    @property
    def downvote_count(self):
        return self.downvoted_by.count()

    @property
    def comment_count(self):
        return Comment.objects.filter(media=self).count()


class MediaVisit(models.Model):
    ip = models.CharField(max_length=40)
    media = models.ForeignKey(Media, on_delete=models.CASCADE)
    user = models.ForeignKey(OrpheusUser, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.ip


class Comment(models.Model):
    parent = models.IntegerField(_("Parent"), null=True)
    created = models.DateTimeField(_("Created"), default=now)
    modified = models.DateTimeField(_("Modified"), null=True)
    content = models.TextField(_("Content"))
    fullname = models.TextField(_("Full name"))
    profile_url = models.TextField(_("Profile url"), blank=True)
    profile_picture_url = models.TextField(_("Profile picture url"),
                                           blank=True)
    created_by_admin = models.BooleanField(_("Created by admin"),
                                           default=False)
    created_by_current_user = models.BooleanField(_("Created by current user"),
                                                  default=False)
    upvote_count = models.IntegerField(_("Upvote count"), default=0)
    user_has_upvoted = models.BooleanField(_("User has upvoted"), default=False)
    media = models.ForeignKey(Media, on_delete=models.CASCADE)
    user = models.ForeignKey(OrpheusUser, on_delete=models.CASCADE)
    upvoted_by = models.ManyToManyField(OrpheusUser,
                                        blank=True,
                                        related_name="comment_upvoted_by")

    def __str__(self):
        return "%s %s %s" % (self.pk, self.created, self.user.username)
