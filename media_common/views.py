from django.core.cache import cache
from django.db.models import Q, Count
from django.http import HttpResponse
from django.urls import reverse_lazy, reverse
from django.http import Http404
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from pure_pagination import PaginationMixin
from rest_framework import status
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView
)
from rest_framework.response import Response
from .models import Comment, Media, MediaVisit
from .serializers import CommentSerializer


class MediaIndexView(generic.ListView):

    def get_queryset(self):
        media = Media.objects.all()
        return MediaIndexView.get_frontpage_media(media)

    @staticmethod
    def get_frontpage_media(media, page=1, count=21):
        # Sort by media properties in reversed order of importance
        # sorting with the most important condition last
        media = sorted(media, key=lambda m: m.comment_count, reverse=True)
        media = sorted(media, key=lambda m: m.downvote_count)
        media = sorted(media, key=lambda m: m.upvote_count, reverse=True)
        media = sorted(media, key=lambda m: m.view_count, reverse=True)
        end = page * count
        start = end - count
        return media[start:end]


class MediaDetailView(generic.DetailView):

    def count_views(self):
        # Set nginx header for remote IPs in production
        if 'HTTP_X_REAL_IP' in self.request.META:
            self.request.META['REMOTE_ADDR'] = (
                self.request.META['HTTP_X_REAL_IP'])

        ip = self.request.META['REMOTE_ADDR']
        user = 'anonymous'
        if self.request.user.is_authenticated:
            user = self.request.user.username
        cache_key = "%s:%s:%s" % (self.object.id, user, ip)
        has_viewed = cache.get(cache_key)
        if not has_viewed:
            # Set cache to keep records of ip addresses for an hour
            # to achieve a semi unique view counter
            cache.set(cache_key, True, 60 * 60)
            visit = MediaVisit(ip=ip, media=self.object)
            if self.request.user.is_authenticated:
                visit.user = self.request.user
            visit.save()
        return self.object.view_count

    def get_context_data(self, **kwargs):
        context = super(MediaDetailView, self).get_context_data(**kwargs)
        upvotes = self.object.upvoted_by.filter(pk=self.request.user.pk)
        downvotes = self.object.downvoted_by.filter(pk=self.request.user.pk)
        vote = "none"
        if self.request.user in upvotes:
            vote = 'upvote'
        elif self.request.user in downvotes:
            vote = 'downvote'
        context['current_user_vote'] = vote
        context['upvote_count'] = self.object.upvote_count
        context['downvote_count'] = self.object.downvote_count
        context['view_count'] = self.count_views()
        return context


class MediaSearchView(PaginationMixin, generic.ListView):
    context_object_name = "results"
    paginate_by = 12

    def get_queryset(self):
        query = self.request.GET['query']
        search_keys = query.split()
        result_lists = []
        for key in search_keys:
            result_lists.append(
                self.model.objects.filter(
                    Q(title__icontains=key) |
                    Q(user__username__icontains=key) |
                    Q(user__full_name__icontains=key) |
                    Q(tags__name__in=[key])
                )
            )
        # Create empty queryset and chain results for all search keys
        results = self.model.objects.none()
        for result_list in result_lists:
            results |= result_list

        # Order results by order of appearances in results
        # therefore getting the most relevant result and
        # then exclude all duplicates
        results = (results.annotate(itemcount=Count('id'))
                   .order_by('-itemcount')
                   .distinct())
        return results


def media_upvote_view(request, media_id):
    if not request.user.is_authenticated.value:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    media = Media.objects.get(pk=media_id)
    upvotes = media.upvoted_by.filter(pk=request.user.pk)
    downvotes = media.downvoted_by.filter(pk=request.user.pk)
    if request.user not in upvotes:
        if request.user in downvotes:
            media.downvoted_by.remove(request.user)
        media.upvoted_by.add(request.user)
    else:
        media.upvoted_by.remove(request.user)
    media.save()
    return HttpResponse(status=status.HTTP_200_OK)


def media_downvote_view(request, media_id):
    if not request.user.is_authenticated.value:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    media = Media.objects.get(pk=media_id)
    upvotes = media.upvoted_by.filter(pk=request.user.pk)
    downvotes = media.downvoted_by.filter(pk=request.user.pk)
    if request.user not in downvotes:
        if request.user in upvotes:
            media.upvoted_by.remove(request.user)
        media.downvoted_by.add(request.user)
    else:
        media.downvoted_by.remove(request.user)
    media.save()
    return HttpResponse(status=status.HTTP_200_OK)


class MediaCreate(CreateView):
    model = Media
    fields = ['title', 'description', 'tags']

    def form_valid(self, form):
        media = form.save(commit=False)
        media.user = self.request.user
        media.save()
        # Without this next line the tags won't be saved.
        form.save_m2m()
        if self.request.is_ajax():
            return HttpResponse(media.get_absolute_url())
        else:
            return super(MediaCreate, self).form_valid(form)


class MediaUpdate(UpdateView):
    model = Media
    fields = ['title', 'description', 'tags']

    def form_valid(self, form):
        media = form.save(commit=False)
        media.save()
        # Without this next line the tags won't be saved.
        form.save_m2m()
        if self.request.is_ajax():
            return HttpResponse(media.get_absolute_url())
        else:
            return super(MediaUpdate, self).form_valid(form)

    def get_object(self, queryset=None):
        media = super(MediaUpdate, self).get_object()
        if (not media.user == self.request.user and
                not self.request.user.is_staff):
            raise Http404
        return media


class MediaDelete(DeleteView):
    model = Media

    def get_success_url(self):
        return reverse_lazy('account_profile',
                            kwargs={'username': self.object.user.username})

    def get_object(self, queryset=None):
        media = super(MediaDelete, self).get_object()
        if (not media.user == self.request.user and
                not self.request.user.is_staff):
            raise Http404
        return media


class CommentListAPIView(ListAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        media_id = self.kwargs['media_id']
        comments = Comment.objects.filter(media_id=media_id)
        comments = comments.filter(
            Q(parent__in=comments) |
            Q(parent__isnull=True)
        )
        for comment in comments:
            comment.created_by_current_user = (
                comment.user == self.request.user
            )
            comment.created_by_admin = comment.user.is_admin
            comment.user_has_upvoted = (
                self.request.user in
                comment.upvoted_by.filter(pk=self.request.user.pk)
            )
            comment.profile_picture_url = comment.user.profile_picture_url
            comment.profile_url = reverse(
                'account_profile',
                kwargs={'username': comment.user.username}
            )
        queryset = comments
        return queryset


class CommentCreateApiView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class CommentUpdateAPIView(UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class CommentDeleteAPIView(DestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def perform_destroy(self, instance):
        if instance.user == self.request.user or self.request.user.is_staff:
            return super(CommentDeleteAPIView, self).perform_destroy(instance)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


def comment_upvote_view(request, comment_id):
    if not request.user.is_authenticated.value:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    comment = Comment.objects.get(pk=comment_id)
    queryset = comment.upvoted_by.filter(pk=request.user.pk)
    if request.user not in queryset:
        comment.upvoted_by.add(request.user)
        comment.user_has_upvoted = True
        comment.upvote_count += 1
        comment.save()
        return HttpResponse(status=status.HTTP_200_OK)
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


def comment_downvote_view(request, comment_id):
    if not request.user.is_authenticated.value:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    comment = Comment.objects.get(pk=comment_id)
    queryset = comment.upvoted_by.filter(pk=request.user.pk)
    if request.user in queryset:
        comment.upvoted_by.remove(request.user)
        comment.user_has_upvoted = False
        comment.upvote_count -= 1
        comment.save()
        return HttpResponse(status=status.HTTP_200_OK)
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
