from django.http import Http404
from django.urls import reverse
from rest_framework import serializers
from .models import Media, Comment
from django.utils.timezone import now


class CommentSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('_pk')
    media_id = serializers.PrimaryKeyRelatedField(queryset=Media.objects.all())

    class Meta:
        model = Comment
        fields = (
            'id',
            'parent',
            'created',
            'modified',
            'content',
            'fullname',
            'profile_url',
            'profile_picture_url',
            'created_by_admin',
            'created_by_current_user',
            'upvote_count',
            'user_has_upvoted',
            'media_id',
            'user_id',
        )

    def _pk(self, obj):
        return obj.pk

    def create(self, validated_data):
        if not self.context['request'].user.is_authenticated:
            raise Http404
        validated_data['media_id'] = validated_data['media_id'].pk
        validated_data['user_id'] = self.context['request'].user.pk
        validated_data['created'] = now()
        validated_data['modified'] = None
        username = self.context['request'].user.username
        validated_data['profile_url'] = reverse('account_profile',
                                                kwargs={'username': username})
        validated_data['fullname'] = self.context['request'].user.full_name
        return super(CommentSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if not self.context['request'].user.is_authenticated:
            raise Http404
        validated_data['media_id'] = validated_data['media_id'].pk
        validated_data['modified'] = now()
        return super(CommentSerializer, self).update(instance, validated_data)
