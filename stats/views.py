from django.http import Http404
from django.views.generic import TemplateView
from . import stats


class StatsIndexView(TemplateView):
    template_name = 'stats/index.html'

    def get_context_data(self, **kwargs):
        context = super(StatsIndexView, self).get_context_data(**kwargs)
        username = context['username']
        if (not username == self.request.user.username and
                not self.request.user.is_staff):
            raise Http404
        context['media_stats'] = stats.media_stats(username=username)
        return context


class StatsDetailView(TemplateView):
    template_name = 'stats/detail.html'

    def get_context_data(self, **kwargs):
        context = super(StatsDetailView, self).get_context_data(**kwargs)
        username = context['username']
        if (not username == self.request.user.username and
                not self.request.user.is_staff):
            raise Http404
        media_id = context['media_id']
        context['media_detail'] = stats.media_detail(media_id=media_id)
        return context
