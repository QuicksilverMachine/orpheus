from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<username>[\w.@+-]+)$',
        views.StatsIndexView.as_view(),
        name='index'),
    url(r'^(?P<username>[\w.@+-]+)/(?P<media_id>[0-9]+)$',
        views.StatsDetailView.as_view(),
        name='detail'),
]
