from IPy import IP
from django.contrib.gis.geoip2 import GeoIP2
from django.utils.translation import ugettext_lazy as _
from accounts.models import OrpheusUser
from media_common.models import MediaVisit, Media
from music.models import Album
from videos.models import Video


def all_media_stats(all_media):
    result = []
    for media in all_media:
        result_row = {
            'id': media.pk,
            'title': media.title,
            'created': media.created,
            'view_count': media.view_count,
            'upvote_count': media.upvote_count,
            'downvote_count': media.downvote_count,
            'comment_count': media.comment_count,
        }
        result.append(result_row)
    result = sorted(result, key=lambda row: row['id'])
    return result


def media_stats(username):
    user = OrpheusUser.objects.get(username=username)
    all_videos = Video.objects.filter(user=user)
    all_albums = Album.objects.filter(user=user)
    video_stats = all_media_stats(all_videos)
    album_stats = all_media_stats(all_albums)
    result = {
        'videos': video_stats,
        'music': album_stats
    }
    return result


def media_detail(media_id):
    media = Media.objects.get(pk=media_id)
    visits = MediaVisit.objects.filter(media=media)
    geolocation = GeoIP2()
    result = all_media_stats([media])[0]
    result['geolocation'] = []
    countries = []
    for visit in visits:
        ip_type = IP(visit.ip).iptype()
        if ip_type == 'PRIVATE':
            country = {
                'country_name': _('Local network'),
                'country_code': 'LAN'
            }
        else:
            country = geolocation.country(visit.ip)
        if country not in countries:
            countries.append(country)
            result['geolocation'].append({
                'country_code': country['country_code'],
                'country_name': country['country_name'],
                'view_count': 1
            })
        else:
            [item for item in result['geolocation'] if item['country_code'] ==
                country['country_code']][0]['view_count'] += 1
    result['geolocation'] = sorted(result['geolocation'],
                                   key=lambda row: row['view_count'],
                                   reverse=True)
    return result
