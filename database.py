#!/usr/bin/python3
import argparse
import os
import shutil
import django
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.cache import cache
from django.core.files import File
from django.urls import reverse
from faker import Factory

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orpheus.settings")
django.setup()

fake = Factory.create()


def clear_cache():
    cache.clear()
    print("Cache cleared.")


def delete_users():
    from accounts.models import OrpheusUser
    OrpheusUser.objects.all().delete()
    dir_path = "media/accounts/profile_pictures/"
    user_list = next(os.walk(dir_path))[1]
    for user in user_list:
        shutil.rmtree(dir_path + user)


def delete_videos():
    from videos.models import Video
    Video.objects.all().delete()

    dir_path = "media/videos/videos/"
    video_list = next(os.walk(dir_path))[1]
    for video in video_list:
        shutil.rmtree(dir_path + video)

    dir_path = "media/videos/captions/"
    caption_list = next(os.walk(dir_path))[1]
    for caption in caption_list:
        shutil.rmtree(dir_path + caption)


def delete_music():
    from music.models import Album
    Album.objects.all().delete()
    dir_path = "media/music/albums/"
    album_list = next(os.walk(dir_path))[1]
    for album in album_list:
        shutil.rmtree(dir_path + album)


def reset_users():
    print("Resetting users...")
    from accounts.models import OrpheusUser

    delete_users()

    OrpheusUser.objects.create_superuser(
        username='admin',
        full_name='Administrator',
        email='quicksilver.machine@gmail.com',
        password='admin')
    for i in range(1, 11):
        user = OrpheusUser.objects.create_user(
            username=fake.user_name(),
            full_name=fake.name(),
            email="user%s@example.com" % i,
            password="password")
        user.save()


def reset_videos():
    print("Resetting videos...")
    from accounts.models import OrpheusUser
    from videos.models import Video

    delete_videos()

    users = OrpheusUser.objects.all()[1:]
    for i in range(1, 201):
        video = Video()
        video.title = fake.file_name().split('.')[0].capitalize()
        video.description = fake.text()
        video.user = users[(i - 1) % users.count()]
        video.save()
    for video in Video.objects.all():
        video_file = video.source
        with open('media_test/video.mp4', 'rb') as file:
            video_file.save(name='%s.mp4' % video.pk,
                            content=File(file),
                            save=True)
        caption_file = video.captions
        with open('media_test/video.srt', 'rb') as file:
            caption_file.save(name='%s.srt' % video.pk,
                              content=File(file),
                              save=True)


def reset_music():
    print("Resetting music...")
    from accounts.models import OrpheusUser
    from music.models import Album, Song

    delete_music()

    users = OrpheusUser.objects.all()[1:]
    for i in range(1, 201):
        album = Album()
        album.title = fake.file_name().split('.')[0].capitalize()
        album.description = fake.text()
        album.user = users[(i - 1) % users.count()]
        album.save()
        for j in range(1, 11):
            song = Song(
                track_number=j,
                title=fake.file_name().split('.')[0].capitalize(),
                album=album,
            )
            song.save()
    for song in Song.objects.all():
        song_file = song.source
        if song.id % 2:
            filename = "song1.mp3"
        else:
            filename = "song2.mp3"
        with open('media_test/%s' % filename, 'rb') as file:
            song_file.save(name='%s.mp3' % song.pk,
                           content=File(file),
                           save=True)


def reset_comments():
    print("Resetting comments...")
    from accounts.models import OrpheusUser
    from media_common.models import Media
    from media_common.models import Comment

    Comment.objects.all().delete()

    users = OrpheusUser.objects.all()
    media = Media.objects.all()

    for i in range(1, 501):
        comment_media = media[i % media.all().count()]
        comment_user = users[i % users.count()]
        comment = Comment(
            parent=None,
            content=fake.text(),
            fullname=comment_user.full_name,
            created_by_current_user=False,
            created_by_admin=False,
            upvote_count=i,
            user_has_upvoted=False,
            profile_url=reverse('account_profile',
                                kwargs={'username': comment_user.username}),
            profile_picture_url=static('accounts/img/user-icon.png'),
            user=comment_user,
            media=comment_media,
        )
        comment.save()


def reset_db():
    reset_users()
    reset_videos()
    reset_music()
    reset_comments()
    print("Done.")


def reset_all():
    clear_cache()
    reset_db()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Orpheus database reset operations')
    parser.add_argument('operation',
                        choices=['cache', 'db', 'all'],
                        help="What to reset")
    args = parser.parse_args()
    if args.operation == 'cache':
        clear_cache()
    elif args.operation == 'db':
        reset_db()
    elif args.operation == 'all':
        reset_all()
